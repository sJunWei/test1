/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import axios from 'axios';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username='';
  password='';
  error = null;
  invalidLogin = false;
  errorMessage = 'Please enter correct ';
  constructor(private router: Router) { }

  ngOnInit() {}

  async loginAction(){
    try {
      const data = {
        userName:this.username,
        passwordHash:this.password,
      };
      console.log(data);
      const response = await axios.post('https://localhost:5001/api/auth/login',data);
      console.log(response.data);
      if(response.status === 200){
        this.router.navigate(['/home']);
      }
    } catch (error) {
      this.invalidLogin = true;
    }
  }
}
