import { Component, OnInit } from '@angular/core';
import axios from 'axios';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  title = 'angular-app';
  users = [];
  error = null;

  async ngOnInit() {
    try {
      // const cors = require('cors');
      const response = await axios.get('https://localhost:5001/api/users/getUsers');
      this.users = response.data;
    } catch (error) {
      this.error = error;
    }
  }
}
