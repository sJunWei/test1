USE [CalendarSQL]
GO
/****** Object:  Table [dbo].[calendar]    Script Date: 4/5/2022 4:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calendar](
	[calendarId] [int] NULL,
	[calendarTitle] [varchar](50) NULL,
	[calendarDescription] [varchar](150) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 4/5/2022 4:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[userId] [int] NULL,
	[userFirstName] [varchar](50) NULL,
	[userLastName] [varchar](50) NULL,
	[userName] [varchar](50) NULL,
	[userEmail] [varchar](50) NULL,
	[userMobile] [varchar](13) NULL,
	[passwordHash] [varchar](150) NULL,
	[registerAt] [datetime] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[users] ([userId], [userFirstName], [userLastName], [userName], [userEmail], [userMobile], [passwordHash], [registerAt]) VALUES (1, N'Jun Wei', N'Wong', N'junwei16', N'wongjw122@gmail.com', N'011-13544219', N'1234567890', CAST(N'2022-04-04T15:15:32.000' AS DateTime))
